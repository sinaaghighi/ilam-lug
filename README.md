# Ilam Linux Users Group official website

If you want to contribute in Ilam LUG website you have to:

- Fork this project
- Commit to the project and push your commits to your fork
- Send a merge request to the [maintainer](https://gitlab.com/ilam_lug)

# How to reach IlamLUG:

- [Website](https://ilamlug.ir)
- [Telegram](https://t.me/ilamlug)
- [Twitter](https://twitter.com/IlamLUG)
- [Mastadon](https://mastodon.social/@ilamlug)
- [BigBlueButton Instanace](https://ilamlug.social)
- [Instagram](https://instagram.com/IlamLUG)
- [PeerTube](https://peertube.linuxrocks.online/accounts/ilam_lug)
- [Donation](https://payping.ir/@ilamlug)
- [E-mail](info@ilamlug.ir)
